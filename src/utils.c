#include <assert.h>
#include <stdlib.h>

#include "utils.h"

uint32_t get_random_number( uint32_t min, uint32_t max )
{
    uint32_t ret = ( rand() / ( (double)RAND_MAX + 1 ) * ( max - min + 1 ) + min );

    assert( ret >= min );
    assert( ret <= max );
    return ret;
}

