#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

struct s_elem
{
    struct s_elem *prev;
    void *blob;
};

struct stack
{
    uint32_t elem_count;
    uint32_t elem_size;
    struct s_elem *top;
};

struct stack* stack_alloc( uint32_t sizeof_elem )
{
    struct stack *s = (struct stack*)malloc( sizeof( struct stack ) );
    s->elem_size = sizeof_elem;
    s->top = 0;
    s->elem_count = 0;
    return s;
}

void stack_push( struct stack *s, void *blob )
{
    // TODO: revise this double malloc approach.
    struct s_elem *elem = (struct s_elem*)malloc( sizeof( struct s_elem ) );
    elem->blob = (void*)malloc( s->elem_size );

    elem->prev = s->top;
    s->top = elem;
    s->elem_count++;
    memcpy( elem->blob, blob, s->elem_size );
}

void* stack_pop( struct stack *s )
{
    if( s->elem_count == 0 )
    {
        return 0;
    }

    struct s_elem *elem = s->top;
    void *ret = elem->blob;
    s->top = elem->prev;
    free( elem );
    s->elem_count--;
    return ret;
}

void stack_free( struct stack *s )
{
    while( s->top )
    {
        struct s_elem *elem = s->top;
        s->top = elem->prev;
        free( elem->blob );
        free( elem );
    }
    free( s );
}

bool stack_is_empty( struct stack *s )
{
    if( s->top )
    {
        return false;
    }
    return true;
}

uint32_t stack_elem_count( struct stack *s )
{
    return s->elem_count;
}

// DEBUG
void print_stack( struct stack *s )
{
    printf( "Stack dump:\n" );
    struct s_elem *elem = s->top;
    while( elem )
    {
        printf( "\telem %p\n", elem );
        elem = elem->prev;
    }
}
