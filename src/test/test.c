#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "../stack.h"
#include "../queue.h"

// This has to be checked with valgrind --tool=memcheck --leak-check=full
void test_stack()
{
    printf( "test_stack started...\n" );

    // create a stack of int.
    struct stack *s = stack_alloc( sizeof( int ) );

    // push some elements in the stack.
    for( int i = 0; i < 100; ++i )
    {
        stack_push( s, (void*)&i );
    }

    // free the whole stack.
    stack_free( s );

    // create new stack and push some elements.
    s = stack_alloc( sizeof( long ) );

    for( long i = 0; i < 200; ++i )
    {
        stack_push( s, (void*)&i );
    }

    // pop some elements.
    long *one = (long*)stack_pop( s );
    assert( *one == 199 );

    long *two = stack_pop( s );
    assert( *two = 198 );

    // free the memory returned by the stack.
    free( one );
    free( two );

    // free the whole stack.
    stack_free( s );

    printf( "test_stack finished.\n" );
}

void test_queue()
{
    printf( "test_queue started...\n" );

    // TEST1:
    // create queue of int.
    struct queue *q = queue_alloc( sizeof( int ) );

    // add elements to the queue.
    for( int i = 0; i < 100; ++i )
    {
        queue_enqueue( q, &i );
    }

    // free the whole queue.
    queue_free( q );
    // ~TEST1

    // TEST2
    // create a new queue.
    q = queue_alloc( sizeof( long long ) );

    // add one single element.
    int elem1 = 1;
    queue_enqueue( q, &elem1 );

    int ret1 = *(long long*)queue_dequeue( q );
    assert( ret1 == 1 );

    queue_enqueue( q, &elem1 );

    ret1 = *(long long*)queue_dequeue( q );
    assert( ret1 == 1 );

    // add some elements to the queue.
    for( long long i = 0; i < 100; ++i )
    {
        queue_enqueue( q, &i );
    }

    // check the size of the queue.
    assert( queue_elem_count( q ) == 100 );

    // dequeue some elements and check their values.
    long long *zero = ( long long* )queue_dequeue( q );
    assert( *zero == 0 );

    long long *one = ( long long* )queue_dequeue( q );
    assert( *one == 1 );

    // check the size of the queue.
    assert( queue_elem_count( q ) == 98 );

    // free the dequeued elements.
    free( zero );
    free( one );

    // free the whole queue.
    queue_free( q );
    // ~TEST2

    printf( "test_queue finished." );
}

int main()
{
    test_stack();
    test_queue();
}
