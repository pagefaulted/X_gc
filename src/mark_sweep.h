#ifndef MARK_SWEEP_H
#define MARK_SWEEP_H

#include <stdint.h>

// This is a simple gc algorithm based on the concept of reachability.
// An object is considered garbage if it cannot be reached from the
// roots of the object tree or from the locals on the stack.
// In the first phase the object graph is traversed from the roots
// down. Each object encountered is marked alive. This phase is
// called _tracing_.
// At the end of the tracing phase, all the objects that have not
// been marked are in fact garbage.
// The second phase consists in a full traversal of the whole heap to
// check for objects that have not been marked. Unmarked objects are
// reclaimed. This phase is called _sweeping_.
// Mark-sweep is an _indirect collection_ algorithm. It does not detect
// garbage, it indirecly identifies the garbage by marking the live
// objects.
// Note that mark-sweep introduces constraints for the heap layout:
// the sweeping phase requires the heap to be somehow parsable.
// Objects are not moved, hence the heap is not defragmented and the allocator
// needs to carefully allocate memory to avoid eccessive fragmentation.


void create_obj_tree();
void disconnect_edges();
void dump_object_tree();
void setup_heap( uint32_t heap_size );
void mark_sweep_gc();
void check_gc();
void free_heap();

#endif // MARK_SWEEP_H
