#include <assert.h>
#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <stdint.h>
#include "dotty_gen.h"

static FILE *dot_file = 0;

// Open the graph.dot file.
// Writes the header.
void setup( const char *file_name )
{
    dot_file = fopen( file_name, "w+" );
    if( dot_file == 0 )
    {
        error( 0, errno, "Error opening dot graph file."  );
    }
    else
    {
        int32_t ret = fputs( "digraph g {\n", dot_file );
        if( ret == EOF )
        {
            error( 0, errno, "Error writing to the dot graph file." );
        }
    }
}

// Write the footer.
// Close the graph.dot file.
void tear_down()
{
    int32_t ret = fputs( "}", dot_file );
    if( ret == EOF )
    {
        error( 0, errno, "Error writing to the dot graph file." );
    }

    ret = fclose( dot_file );
    if( ret == EOF )
    {
        error( 0, errno, "Error closing the dot graph file." );
    }
    dot_file = 0;
}

// Writes entries for this node.
// The format is graphviz. a->b
//                         ^ nodename
//                          ^^ edge
//                            ^child
void write_node( struct node *n )
{
    assert( dot_file && "Error, did you call setup before calling this method?\n" );
    for( uint16_t i = 0; i < n->num_children; ++i )
    {
        if( n->children_id[ i ] != 0 )
        {
            fprintf( dot_file, "%ld->%ld;\n", (long)n->id, (long)n->children_id[ i ] );
        }
    }
}
