#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mark_sweep.h"
#include "stack.h"
#include "queue.h"
#include "dotty_gen.h"
#include "utils.h"

struct roots
{
    uint16_t len;
    struct object *obj[];
} *roots;

struct object
{
    uint32_t id;

    // TODO: remove the need for the allocated bit.
    // 7-----10
    //        ^ alive bit
    //       ^ allocated bit
    char header;
    // TODO: change this into a random number [0-10].
    // For simplicity, objects can have only 3 reference fields.
    struct object *children[ 3 ];
};

static char *heap_start   = 0;
static char *heap_end     = 0;
static uint32_t obj_count = 0;

static const char ALIVE_BIT     = 0x01;
static const char ALLOCATED_BIT = 0x02;
static const uint16_t OBJ_SIZE  = sizeof( struct object );

void* new_obj( uint16_t size )
{
    // TODO: this free object search is simple but highly inefficient, improve it!
    // TODO: Fix up all this casting by using the correct type.
    // Go through the objects and assign the first free one.
    char *obj = (char*)heap_start;
    while( obj + OBJ_SIZE <= heap_end )
    {
        if( ~( (struct object*)obj )->header & ALLOCATED_BIT )
        {
            ( (struct object*)obj )->header |= ALLOCATED_BIT;
            return obj;
        }
        obj += OBJ_SIZE;
    }

    // No free space left.
    return 0;
}

static void create_roots( uint16_t roots_count )
{
    roots = (struct roots*)malloc( sizeof( struct roots ) + roots_count *
            sizeof( struct object* ) );

    roots->len = roots_count;

    // Assign objects to roots.
    for( int i = 0; i < roots_count; ++i )
    {
        roots->obj[ i ] = new_obj( 0 );
    }
}

void disconnect_edges()
{
    // This is to simulate objects going out of scope.
    // Garbage 40% of the objects created.
    const uint32_t num_obj_to_garbage = obj_count * 0.4;
    printf( "Disconnect %d objects\n", num_obj_to_garbage );

    srand( time( NULL ) );
    for( int i = 0; i < num_obj_to_garbage; ++i )
    {
        uint32_t obj_num = get_random_number( 4, obj_count - 1 );
        assert( obj_num < obj_count );
        struct object *objects = (struct object*)heap_start;

        ( objects + obj_num )->children[ get_random_number( 0, 2 ) ] = 0;
    }

    obj_count -= num_obj_to_garbage;
}

void create_obj_tree()
{
    // Count how many objects can be fit in the heap.
    uint32_t max_objects_per_root =
        ( ( heap_end - heap_start ) / sizeof( struct object ) ) / roots->len;

    for( int i = 0; i < roots->len; ++i )
    {
        int current_root_obj_count = 0;

        // Each element of the queue is a pointer to the pointer to the object field to
        // be written.
        // Use Breadth First algorithm to create the graph.
        struct queue *q = queue_alloc( sizeof( struct object** ) );
        queue_enqueue( q, &roots->obj[ i ] );

        while( !queue_is_empty( q ) && max_objects_per_root > current_root_obj_count )
        {
            void *obj_pp = queue_dequeue( q );
            struct object *obj = *(struct object**)obj_pp;
            for( int i = 0; i < 3; ++i )
            {
                obj->children[ i ] = new_obj( 0 );
                if( obj->children[ i ] != 0 )
                {
                    ++current_root_obj_count;
                    ++obj_count;
                    queue_enqueue( q, &obj->children[ i ] );
                }
            }
            free( obj_pp );
        }
        queue_free( q );
    }
    printf( "Created %d objects.\n", obj_count );
}

void setup_heap( uint32_t heap_size )
{
    heap_start = (char*)malloc( heap_size );
    heap_end   = heap_start + heap_size;

    uint32_t count = 0;

    // The heap is configured as a linear sequence of objects.
    char *elem = (char*)heap_start;
    while( elem + OBJ_SIZE <= heap_end )
    {
        struct object *object = (struct object*)elem;
        object->id = ++count;
        object->header = 0;

        for( int i = 0; i < 3; ++i )
        {
            object->children[ i ] = 0;
        }

        // Go to next object.
        elem += OBJ_SIZE;
    }

    // TODO: parametrize this.
    create_roots( 3 );
}

void mark_sweep_gc()
{
    // Traverse the object graph and mark live objects using DF algorithm.
    // DF space complexity is at worst O(n) ( d*( b - 1 ) + 1 ),
    // which is much better than BF's O(n^d).

    // mark_phase
    // TODO: stop using this generic stack. Implement stack of pointers.
    struct stack *stk = stack_alloc( sizeof( struct object* ) );

    for( int i = 0; i < roots->len; ++i )
    {
        if( ~roots->obj[ i ]->header & ALIVE_BIT )
        {
            // Mark root here to avoid duplicate insertion in case of loops in the
            // object graph.
            roots->obj[ i ]->header |= ALIVE_BIT;
            stack_push( stk, &roots->obj[ i ] );

            while( !stack_is_empty( stk ) )
            {
                void *obj_pp = stack_pop( stk );
                struct object *obj = *(struct object**)obj_pp;

                for( int i = 0; i < 3; ++i )
                {
                    if( obj->children[ i ] != 0 &&
                            ~obj->children[ i ]->header & ALIVE_BIT )
                    {
                        // As with the roots, mark object here to avoid duplicate
                        // insertion in case of loops in the objects graph.
                        obj->children[ i ]->header |= ALIVE_BIT;
                        stack_push( stk, &obj->children[ i ] );
                    }
                }
                free( obj_pp );
            }
        }
    }

    stack_free( stk );
    // ~mark_phase

    // sweep_phase
    struct object *obj = (struct object*)heap_start;

    while( obj + 1 <= (struct object*)heap_end )
    {
        // Check if it was marked during the mark phase.
        if( ~obj->header & ALIVE_BIT )
        {
            // This object is dead, reclaim it.
            obj->header &= ~ALLOCATED_BIT;
        }

        // Reset alive bit for next mark phase.
        obj->header &= ~ALIVE_BIT;

        obj += 1;
    }
    // ~sweep_phase
}

void dump_object_tree( const char *file_name )
{
    setup( file_name );

    for( int i = 0; i < roots->len; ++i )
    {
        // Use Depth First algorithm to dump the tree to dot file.
        struct stack *s = stack_alloc( sizeof( void* ) );
        stack_push( s, &roots->obj[ i ] );

        while( !stack_is_empty( s ) )
        {
            void *obj_pp = stack_pop( s );
            struct object *obj = *(struct object**)obj_pp;
            struct node *node = (struct node*)malloc( sizeof( struct node ) );
            node->id = obj->id;
            node->num_children = 3;
            node->children_id = calloc( node->num_children, sizeof( uint32_t ) );
            for( int i = 0; i < node->num_children; ++i )
            {
                node->children_id[ i ] =
                    obj->children[ i ] != 0 ? obj->children[ i ]->id : 0;

                if( obj->children[ i ] != 0 )
                {
                    stack_push( s, &obj->children[ i ] );
                }
            }
            write_node( node );
            free( obj_pp );
            free( node->children_id );
            free( node );
        }

        stack_free( s );
    }
    tear_down();
}

// Checks that gc did not deallocate an object that is still reachable from the roots.
void check_gc()
{
    // TODO: the need to go through the whole graph came up few times already.
    //       Implement a function that takes a function pointer and a roots pointer.
    //       The provided function will be executed on any element of the graph
    //       each root belongs to.
    // NOTE: this traversal logic works only for trees. Fix it when moving to a graph.

    struct stack *s = stack_alloc( sizeof( struct object* ) );

    for( int i = 0; i < roots->len; ++i )
    {
        stack_push( s, &roots->obj[ i ] );

        while( !stack_is_empty( s ) )
        {
            void *obj_pp = (struct object**)stack_pop( s );

            struct object *obj = *(struct object**)obj_pp;

            if( ~obj->header & ALLOCATED_BIT )
            {
                printf( "Error, object with id %d should not have been garbaged!\n",
                     obj->id );
            }

            for( int i = 0; i < 3; ++i )
            {
                if( obj->children[ i ] )
                {
                    stack_push( s, &obj->children[ i ] );
                }
            }
            free( obj_pp );
        }
    }
    stack_free( s );
}

void free_heap()
{
    free( heap_start );
}

