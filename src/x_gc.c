#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "dotty_gen.h"
#include "queue.h"
#include "mark_sweep.h"

// X_gc steps:
// 0) Setup the heap.
// 1) Create an object graph and dump it to dot file.
// 2) Disconnect graph edges to simulate objects going out of scope. Dump to dot file.
// 3) Call the available GC algorithms.
// 4) Check the results.
int main()
{
    setup_heap( 1024 * 1024 );
    create_obj_tree();
    //dump_object_tree( "original.dot" );

    disconnect_edges();
    //dump_object_tree( "gced.dot" );

    mark_sweep_gc();
    check_gc();

    disconnect_edges();
    mark_sweep_gc();
    check_gc();

    free_heap();
}
