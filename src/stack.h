#ifndef STACK_H
#define STACK_H

#include <stdbool.h>
#include <stdint.h>

/**
 * This is a simple, inefficient and thread UNsafe implementation of a stack that can
 * store generic elements.
 */
struct stack;

struct stack* stack_alloc( uint32_t sizeof_elem );
void stack_push( struct stack *s, void *elem );
void* stack_pop( struct stack *s );
void stack_free( struct stack *s );
bool stack_is_empty( struct stack *s );
uint32_t stack_elem_count( struct stack *s );

// DEBUG interface

void print_stack( struct stack *s );

#endif // STACK_H
