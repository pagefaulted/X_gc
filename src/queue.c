#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

struct q_elem
{
    struct q_elem *next;
    void *blob;
};

struct queue
{
    struct q_elem *head;
    struct q_elem *tail;
    uint32_t elem_size;
    uint32_t elem_count;
};

struct queue* queue_alloc( uint32_t sizeof_elem )
{
    struct queue *q = (struct queue*)malloc( sizeof( struct queue ) );
    q->head = 0;
    q->tail = 0;
    q->elem_count = 0;
    q->elem_size = sizeof_elem;
    return q;
}

void queue_enqueue( struct queue *q, void *elem )
{
    struct q_elem *e = (struct q_elem*)malloc( sizeof( struct q_elem ) );
    e->next = 0;
    e->blob = malloc( q->elem_size );
    memcpy( e->blob, elem, q->elem_size );

    if( q->tail != 0 )
    {
        q->tail->next = e;
    }
    else
    {
        q->head = e;
        assert( q->elem_count == 0 );
    }
    q->tail = e;
    ++q->elem_count;
}

void* queue_dequeue( struct queue *q )
{
    if( q->elem_count == 0 )
    {
        return 0;
    }

    struct q_elem *elem = q->head;
    q->head = elem->next;

    void *ret = elem->blob;
    free( elem );
    --q->elem_count;

    if( queue_is_empty( q ) )
    {
        q->tail = 0;
    }

    return ret;
}

void queue_free( struct queue *q )
{
    while( q->head != 0 )
    {
        struct q_elem *elem = q->head;
        q->head = elem->next;

        free( elem->blob );
        free( elem );
    }
    free( q );
}

bool queue_is_empty( struct queue *q )
{
    return q->elem_count == 0 ? true : false;
}

uint32_t queue_elem_count( struct queue *q )
{
    return q->elem_count;
}
