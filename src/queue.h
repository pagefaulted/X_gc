#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>
#include <stdint.h>

struct queue;

struct queue* queue_alloc( uint32_t sizeof_elem );
void queue_enqueue( struct queue *q, void *elem );
void* queue_dequeue( struct queue *q );
void queue_free( struct queue *q );
bool queue_is_empty( struct queue *q );
uint32_t queue_elem_count( struct queue *q );

#endif // ~QUEUE_H
