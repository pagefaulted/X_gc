#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

/**
 * Returns a (pseudo) random positive number between min and max.
 *
 * @param min the minimum number to be returned.
 * @param max the maximum number to be returned.
 * @return a (pseudo) random positive number between min and max.
 */
uint32_t get_random_number( uint32_t min, uint32_t max );

#endif // UTILS_H
