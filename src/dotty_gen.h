#ifndef DOTTY_GEN_H
#define DOTTY_GEN_H

#include <stdint.h>

// This interface exposes functions to help in the generation of a DOTTY digraph.
// Dot uses the following adjacency list notation:
// digraph graph_name{
// a->b;
// b->c;
// }

struct node
{
    uint32_t id;
    // TODO: consider using flexible array member.
    uint32_t *children_id;
    uint16_t num_children;
};

void setup( const char *file_name );

void tear_down();

void write_node( struct node *n );

#endif // DOTTY_GEN_H
