X_gc
========

X_gc wants to be a platform to implement and test GC algorithms.
The program creates a random object graph and run several GC algorithms on it.
